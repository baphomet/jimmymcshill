---
layout: post
title:  "Bypassing CryptoBridge KYC"
date:   2019-02-27 00:00:00 +0200
categories: cryptocurrency exchanges
lang: en
---
# Bypassing CryptoBridge KYC

You may have heard through various social media channels that the BitShares based decentralized exchange (DEX) gateway provider CryptoBridge changed its terms of service and asks for KYC (know you customer) identification for **US citizens**.

There are some easy possibilties to "workaround" this necessity, which I will show you in this article.  

Before that I want you to understand how the BitShares DEX ecosystem generally works and why CryptoBridge now was forced by the laws and regulations of the United States Securities and Exchange Commission (SEC) to demand KYC information from its users accessing their services from the United States.

Content:
1. [The BitShares Ecosystem](#bts_ecosystem)
2. [The User Interface](#user_interface)
3. [The Gateways and User Issued Assets](#the_gateways)
4. [Why CryptoBridge wants KYC now](#why_cryptobridge_wants_kyc)
5. [How this works together](#how_this_works_together)
6. [Using VPN to bypass CryptoBridge's KYC](#using_vpn)
7. [Using Gateways without KYC](#using_other_gateways)
8. [What about decentralized Gateways?](#decentralized_gateways)

## The BitShares Ecosystem <a name="bts_ecosystem"></a>
There are a lot of misunderstandings when it comes to BitShares based decentralized exchanges.

![The BitShares DEX ecosystem](/assets/img/2019-02-27-bypassing_cryptobridge_kyc/the_bitshares_dex_ecosystem.jpg)
*The BitShares DEX ecosystem*

## The User Interface <a name="user_interface"></a>
The most common is that people even don't know that the website they are using is simply a rebranded version of the [BitShares UI reference implementation](https://github.com/bitshares/bitshares-ui). It is the open source base of all known BitShares based "DEXs" like [CryptoBridge](https://wallet.crypto-bridge.org), [OpenLedger](https://openledger.io), [RuDEX](https://market.rudex.org) or [HotDEX](https://wallet.hotdex.eu).  
If you open their websites and compare them you may see obvious similarities because of that.  
In the graphic above you can identify these user interfaces in the yellow boxes, like the "CryptoBridge UI". They simply provide a graphical user interface to the BitShares DEX backend, visualized in red in the graphic above. **And all of them use exactly the same backend!**

## The Gateways and User Issued Assets <a name="the_gateways"></a>
The next part may be the most important part. It covers the so called **gateways**. They are visualized in green in the graphic above.

A gateway in the BitShares context provides a service to use an BitShares-foreign asset (literally any asset except BTS) in the BitShares ecosystem. This is necessary because you can't move(for example) a real BTC amount onto the BitShares blockchain, on which the actual trading of the DEX is taking place.  
You need an entity (the gateway) who takes your BTC and credits you a BTC-pegged equivalent amount of an corresponding BitShares-based asset, the so called User Issued Asset, short UIA.  
It can be called an IOU (I owe you) for the real BTC you transfered to the gateway. Simply said: If you deposit BTC your BitShares account gets credited the BTC-UIA, if you withdraw it the withdrawed amount is discounted from your BTC-UIA balance and so on.

These UIAs can be identified by their prefixes. A BTC-UIA issued by the CryptoBridge gateway is called **BRIDGE**.BTC. The OpenLedger gateway calls it **OPEN**.BTC, HotDEX uses **HOTDEX**.BTC and so on.

Each gateway stores the "real asset" in a custody pool in "real wallets" of the corresponding asset.

## Why CryptoBridge wants KYC now <a name="why_cryptobridge_wants_kyc"></a>
As the BitShares DEX is decentralized, there is no central institution which can apply for an official exchange and trading licence. And there is no central institution which can enforce regulations by exchange comissions.

But we have seen that we need these gateways to get tradable assets into the exchange platform. These gateways are (yet) not decentralized.

Operating a gateway is time and money consuming and so needs a company or institution with some employees managing this. This company is juridically seizable and has to comply with the laws in force. This is the reason why CryptoBridge has chosen to go safe and be compliant. *Better safe than sorry*.

Without KYC it would be very likely that government authorities could shutdown the gateway servers any moment without any pre-announcement. And no one of us really wants to lose his funds. In my oppinion this only makes CryptoBridge more professional.

But I understand the disappointment of some of you crypto fellows. So let's have a look at what we learned and how we can use this!

## How this works together <a name="how_this_works_together"></a>
You now know that all the BitShares based "DEXs" are in truth only gateways into the actual BitShares DEX, which is then operated decentralized on hundreds of BitShares nodes all over the world on the BitShares blockchain. You also know that nearly all of those known DEX websites are simply rebranded BitShares UIs connecting to the BitShares DEX in the background.

So you may ask whether it isn't possible to trade the UIAs of different gateways against each other?

The answer is **yes, it is possible!**  
And there is nothing preventing you from doing so, except possibly non existend liquidity in such a market.

For example you can trade the OPEN.BTC of OpenLedger against the BRIDGE.BTC of CryptoBridge by simply opening the corresponding market:

https://wallet.crypto-bridge.org/market/OPEN.BTC_BRIDGE.BTC  
or  
https://wallet.bitshares.org/#/market/BRIDGE.BTC_OPEN.BTC

Even if the market is not officially listed in the UI market table, you can theoretically trade **any** BitShares UIA against **any** other UIA. As already said, the only limitation may be missing liquidity in such an inofficial market.

## Using VPN to bypass CryptoBridge's KYC <a name="using_vpn"></a>
The following steps describe in short how you easily bypass even an already accepted KYC:

- Use a virtual private network (VPN) service which is not located in the USA. This way CryptoBridge can not make any conclusions about your actual position and can not refuse the service.
- Create a new BitShares account with the VPN connection and accept the terms of service with answering the "Are you a U.S. citizen?" question with "No".
- Use the "Send" feature of the UI to send your funds from the old account with active KYC-demand to the new "Non-US" account on the BitShares blockchain. Because this is done on the BitShares blockchain it's nearly free.
- Use CryptoBridge with your new account via VPN and without KYC.

## Using Gateways without KYC <a name="using_other_gateways"></a>
If you don't want to use a VPN you can simply use another gateway provider which does **not** demand you to do KYC before depositing or withdrawing assets!
For example:
- Use the OpenLedger gateway to deposit BTC. You then get credited OPEN.BTC.
- Exchange the OPEN.BTC to BRIDGE.BTC and go on trading on CryptoBridge with BRIDGE.XXX pairs.
- If you want to withdraw the BTC again, exchange the BRIDGE.BTC to OPEN.BTC and withdraw them via the OpenLedger gateway.

## What about decentralized Gateways? <a name="decentralized_gateways"></a>
Decentralized gateways would "solve" the legal problem inherently, and as far as I know such gateways are already in the making.