---
layout: post
title:  "Warum Kryptowährungen einen schlechten Stand haben"
date:   2019-09-30 00:00:00 +0200
categories: general
---

# Warum Kryptowährungen einen schlechten Stand haben
Eigentlich sollte der Titel "*Meine Meinung,* warum Kryptowährungen einen schlechten Stand" haben lauten. Denn meiner Meinung nach ist es fundamental betracht sehr fragwürdig, ob die aktuell verfügbaren echten public Ledger Währungen Mainstreamfähig sind.  

Nach nun 10 Jahren seit dem Bitcoin Core Code Release, diversen Abspaltungen und "revolutionären" neuen Ideen aus denen tausende von alternativen Kryptowährungen resultierten, ist es noch immer nicht möglich, dass die moderne Oma Müller, die beispielsweise Online-Zahlungen mit PayPal durchführt, einfach so ein Bitcoin Wallet betreiben und bedienen kann. Ohne sich intensivst mit dem Thema zu befassen versteht sich.  
Für mich ist das ein kompletter Fail und hat mich zum Nachdenken angeregt. Gelegentlich sollte man versuchen, über den eigenen Tellerrand hinaus zu blicken, um zu verstehen wie Menschen über eine Thematik denken, die sich eben **nicht** intensiv mit dieser Thematik befasst haben.  

10 Jahre sind eine Menge Zeit, wenn man die Geschwindigkeit der sonstigen technologischen Entwicklung vergleicht. Beispiel: Vor 10 Jahren kam Microsofts Windows 7 und das Apple iPhone 3GS auf den Markt.  
In all dieser Zeit gab es in der Welt der Kryptowährungen jedoch quasi keinerlei Fortschritte im Bezug auf Usability und User Experience. Es gibt zwar super schöne und bunte Wallets für alle möglichen Endgeräte, aber für die Nutzung muss der Anwender trotzdem sehr viele detaillierte Informationen aufnehmen und verarbeiten.  
Und das ist, meiner Meinung nach, gar kein gutes Zeichen!  

Es macht nur mehr deutlich, dass echte public Ledger eine inhärente Komplexität besitzen, die sich nicht einfach durch ein schönes grafisches Benutzerinterface wegabstrahieren lässt!  

Der Ottonormal-Internet-Nutzer ist heutzutage nicht einmal in der Lage sich ein sicheres Facebook Passwort auszusuchen, geschweige denn dieses sicher zu verwahren. Es wäre unzumutbar einer solchen Person die vollständige technische Verantwortung über sein Hab und Gut zu überlassen, auch wenn dies nur bedeutet, die kryptographischen Schlüssel irgendwie zu sicher zu lagern.  

Als Lösung des Problems wäre ein **Teil**-Zentralisierter Dienstlesiter für die Masse vorstellbar.  
Etwa klassiche Banken, die für den Anwender das komplette Wallet-Management übernehmen:
- Wallet-Updates (allen voran kritische Updates)
- Synchronisation der Blockchain
- Pflege von Peer-Nodes
- Überwachung und Reaktion auf Chain-Forks
- Verwahrung der kryptographischen Schlüssel
- und so weiter und so fort.

Banken verfügen seit Jahren über eine IT-Infrastruktur die die scharfen Regeln des *Bundesamts für Sicherheit in der Informationstechnik* (BSI) umsetzen muss. Zudem verfügen sie (hoffentlich) über entsprechend ausgebildetes IT-Personal.  
Für eine etablierte Bank solle es daher kein allzu großes Problem darstellen, eine Kryptowährung völlig Transparent zur Nutzung des Euros als Zahlungsmittel in ihre Geschäftsprozesse zu integrieren.  
Der Anwender nutzt dann, wie gehabt, die Dienste seiner Bank über eine App oder Online Banking. Beim Verlust seiner Zugangsdaten kann er sich über persönliche Kanäle zur Bank mittels offizieller Ausweisdokumente identifiezieren lassen, um neue Zugagsdaten zu erhalten. Da diese Zugangsdaten unabhängig von den kryptographischen Schlüsseln der Kryptowährung sind, besteht hier kein Risiko zum Totalverlust.  

Natürlich widerspricht dies komplett dem, was sich die Blockchain-Hardliner vorstellen. Aber jeder Mensch könnte dann frei entscheiden, ob er solch einen Dienstleister nutzen möchte oder alles selbst in die Hand nimmt, mit samt dem vollen Risiko.  
Wenn jedoch **jeder**, der z.B. Bitcoin nutzen möchte, vollumfänglich für die technischen Einzelheiten verantwortlich ist, wird sich diese Technologie außerhalb von gewissen Randbereichen nie durchsetzen.  

Angenommen, nur mal zum Spaß, Hintz und Kuntz benutzen von Heute an alle ein Bitcoin Wallet. Nun fällt den Bitcoin Entwicklern plötzlich ein heftiger Software-Fehler auf, der ein sofortiges Update der Wallet-Software erfordert. Eventuell verursacht die notwendige Änderung sogar einen Hardfork.  
Wie erklärt man das den Nutzern? Wie stellt man sicher, dass die Hälfte beim Update nicht alles falsch macht?  Solch eine Notlage ruft dann natürlich auch sofort wieder jede Menge Betrüger auf den Plan, die dir beim Update des Wallets freundlicher Weise unter die Arme greifen wollen.  

Ich bin absolut kein Fan davon, aber um ehrlich zu sein glaube ich, dass nur eine zentralisierte Lösung wie Facebooks Libra oder gar Ripple den Durchbruch im Mainstream schaffen wird, weil eben nur solche Lösungen die technische Komplexität vor dem Nutzer verbergen bzw. abstrahieren können.  
Sie ziehen zwar wieder eine zusätzlichen Kontroll- und Vertrauensschicht in das System ein und mach den Grundgedanken von DLTs kaputt. Aber hey...wir nutzen und vertrauen vermutlich alle einer staatlich regulierten Bank, und die wenigstens von uns werden jemals ernsthafte Probleme bei Zahlungsvorgängen erlebt haben.  

Langfristig werden echte dezentrale Währungen dann eher eine Randökonimie bilden, die man auf gängigen Börsen gegen die "Mainstream Währungen" tauschen kann...eben der klassiche Devisenhandel. Aber als praktisches Zahlungsmittel werden sie sich nicht durchsetzen.  

In meinen Augen ist die Genialität von Nakamotos Idee auch genau ihr eigenes Todesurteil. Nämlich die notwenige technische Komplexität, die man nicht einfach wegnehmen kann.
