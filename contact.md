---
title: /contact
layout: page
permalink: /contact
---

# Kontakt
Sende mir am besten eine private Nachricht auf Twitter.

## PGP
Für verschlüsselte Nachrichten nutze bitte den folgenden PGP Key:

```
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: BCPG C# v1.6.1.0

mQENBF2PeckBCACXzEJxVV8XrIGGJko44j5iLf/R+AHDXLQxIWpRsdL168y/WVme
2wJM03iSmpZgNtVgKRyqcYXSNvqvnnpoqNRH3rOzS7q/A4xb0DAxpZz9DfRrZEei
CO6x9nMWRQFbkQY5R3/nYRBxoqmQKKZlFlDRoxqgcVTh9qrvLB9S6+TSIwkV/oHK
vOM07GS4pLIwW/pB/K9ZoGP5jq3PGLoyGkNhd42Bzf9X77P7BYO+PaK7+FZIPgh8
izpR1oG0qKCEFexQU6CdS/SkqG+CvmCWL528O+XDLVIp+aG7Pmk3/eXGYeTmaAd9
Ine0cI2kzPTrcE3RmfyW5QFxs7zstvllhXiHABEBAAG0AIkBHAQQAQIABgUCXY95
yQAKCRDrdfm1yPz+g8wlCACTHQ6L/b+4ClnHnfm5dPrjUMywgcdPXnBYhnFO5coo
ie9hOrGxp5lSar0EluYJSTnrNtY1xBf00AOrw8KSDKs34PT9u3jKUxjeOxUUlGvv
rvxaLSYCY4UHSV2rXtFIYNMAcQrPE3+PPRULUBnIgZdz8Vw3Hf71FD6Pw1zyvnaf
CMxWaXE7DGSOXLC2C+6BoJWLbDEzwx3e9p1r0BYSd2lfYW6NbSS5fjTL1s+3Dj1f
AHlQ0sARCokXHZis9MG200P8xyXxbAL7Vm3xgEmMVHGsAtz2XhDxoZt81NLEbH/H
jJYE63aSWr+1giv5cHwtHiJHFXuAHN/MPrw7uNMxYx7G
=hi6i
-----END PGP PUBLIC KEY BLOCK-----
```
