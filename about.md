---
layout: page
title: /about
permalink: /about/
---

# Mein Alias
Der Name "JimmyMcShill" ist ein von James Morgan "Jimmy" McGill alias Saul Goodman - Hauptcharakter der TV Serie "Better Call Saul" - inspirierter Alias.

# Credits
The website is built with [jekyll](https://github.com/jekyll/jekyll).  
The website theme is [jekyll-theme-console](https://github.com/b2a3e8/jekyll-theme-console)

